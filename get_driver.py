#
# import os
#
# proxy = 'http://EBU8KA:Uj7VxTO0pb@45.89.19.38:6696'
#
# os.environ['http_proxy'] = proxy
# os.environ['HTTP_PROXY'] = proxy
# os.environ['https_proxy'] = proxy
# os.environ['HTTPS_PROXY'] = proxy
#
#
# from selenium import webdriver
#
#
# PROXY = "http://45.89.19.38:6696" # IP:PORT or HOST:PORT
#
# chrome_options = webdriver.ChromeOptions()
# chrome_options.add_argument('--proxy-server=%s' % PROXY)
#
# chrome = webdriver.Chrome(chrome_options=chrome_options)
# chrome.execute_async_script("")
# chrome.get("https://www.myip.com/")
# alert_obj = chrome.switch_to.alert
# a = chrome.execute_script("return prompt('Enter smth','smth')")
# print ("got back %s" % a)


# from selenium import webdriver
# from selenium.webdriver.chrome.options import Options
#
# chrome_options = Options()
# chrome_options.add_extension("proxy.zip")
#
# driver = webdriver.Chrome(executable_path='chromedriver.exe', chrome_options=chrome_options)
# driver.get("https://www.myip.com/")
# driver.get("https://www.myip.com/")
# input()


import os
import zipfile

from selenium import webdriver


def get_proxy(i):
    # PROXY = None  # IP:PORT or HOST:PORT
    with open('resourse_files/proxyes.txt', 'r', encoding='utf-8') as f:
        return f.readlines()[i]


def get_chromedriver(use_proxy=False, user_agent=None, i=0):
    PROXY_HOST = get_proxy(i).split("@")[1].split(":")[0]  # rotating proxy or host
    PROXY_PORT = int(get_proxy(i).split("@")[1].split(":")[1].replace("\n", ""))  # port
    PROXY_USER = 'EBU8KA'  # username
    PROXY_PASS = 'Uj7VxTO0pb'  # password

    manifest_json = """
    {
        "version": "1.0.0",
        "manifest_version": 2,
        "name": "Chrome Proxy",
        "permissions": [
            "proxy",
            "tabs",
            "unlimitedStorage",
            "storage",
            "<all_urls>",
            "webRequest",
            "webRequestBlocking"
        ],
        "background": {
            "scripts": ["background.js"]
        },
        "minimum_chrome_version":"22.0.0"
    }
    """

    background_js = """
    var config = {
            mode: "fixed_servers",
            rules: {
            singleProxy: {
                scheme: "http",
                host: "%s",
                port: parseInt(%s)
            },
            bypassList: ["localhost"]
            }
        };

    chrome.proxy.settings.set({value: config, scope: "regular"}, function() {});

    function callbackFn(details) {
        return {
            authCredentials: {
                username: "%s",
                password: "%s"
            }
        };
    }

    chrome.webRequest.onAuthRequired.addListener(
                callbackFn,
                {urls: ["<all_urls>"]},
                ['blocking']
    );
    """ % (PROXY_HOST, PROXY_PORT, PROXY_USER, PROXY_PASS)
    path = os.path.dirname(os.path.abspath(__file__))
    chrome_options = webdriver.ChromeOptions()
    if use_proxy:
        pluginfile = 'proxy_auth_plugin.zip'

        with zipfile.ZipFile(pluginfile, 'w') as zp:
            zp.writestr("manifest.json", manifest_json)
            zp.writestr("background.js", background_js)
        chrome_options.add_extension(pluginfile)
    if user_agent:
        chrome_options.add_argument('--user-agent=%s' % user_agent)
    driver = webdriver.Chrome(
        os.path.join(path, 'chromedriver'),
        chrome_options=chrome_options)
    return driver, PROXY_HOST + ":" + str(PROXY_PORT)


def main():
    for i in range(10):
        driver = get_chromedriver(use_proxy=True, i=i)
        driver.get('https://www.myip.com/')
    input()


if __name__ == '__main__':
    main()
