from selenium import webdriver
from selenium.common.exceptions import WebDriverException, NoSuchElementException, ElementClickInterceptedException
from selenium.webdriver.common.keys import Keys
import time
import random
import string
import os
import datetime
import threading


def get_email(line):
    try:
        with open('resourse_files/mailru.txt') as f:
            return f.readlines()[line].split(';')  # returns: email, pass
    except IndexError:
        print('Out of emails!')
        return False

def get_works_emails():
    with open('resourse_files/mailru_works.txt', 'r', encoding='utf-8') as f:
        return f.readlines()


def check(deep=False):
    """deep: flags"""
    browser = webdriver.Chrome(executable_path='chromedriver.exe')
    browser.get('https://mail.ru/')
    line = 0
    ok = 0
    while get_email(line):
        email = get_email(line)
        browser.find_element_by_id('mailbox:login').send_keys(email[0])
        browser.find_element_by_id('mailbox:submit').click()
        time.sleep(0.5)
        if browser.find_element_by_id('mailbox:error').text != 'Неверное имя ящика':

            # check with authorization
            if deep > 0:
                browser.find_element_by_id('mailbox:password').send_keys(email[1])
                time.sleep(3)
                if browser.find_element_by_class_name('page'):
                    try:
                        browser.find_element_by_id('PH_logoutLink').click()
                    except ElementClickInterceptedException:
                        browser.find_element_by_class_name('c0120').click()
                        browser.find_element_by_id('PH_logoutLink').click()
                    browser.get('https://mail.ru/')
                    deep = 2
                    print("ENTER")

            if deep is False or deep == 2:
                ok += 1
                print('{} OK'.format(email[0]))
                account = email[0] + ';' + email[1]
                accounts = get_works_emails()
                with open('resourse_files/mailru_works.txt', 'a', encoding='utf-8') as f:
                    if account not in accounts:
                        f.write(email[0] + ';' + email[1])
        else:
            print('{} NOT'.format(email[0]))
        browser.delete_all_cookies()
        browser.refresh()
        line = line + 1
    print("Work accounts: {}".format(ok))
    # open('mailru', 'w').close()



check(True)
print(time.process_time())