# WORK METHOD!!!
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
import time
import random
import string
import os
import datetime
from random import randint

from get_driver import get_chromedriver
from autoreg_mail import random_word


# PROXY = "178.213.13.136:53281" # IP:PORT or HOST:PORT
#
# chrome_options = webdriver.ChromeOptions()
# chrome_options.add_argument('--proxy-server=%s' % PROXY)
#
# chrome = webdriver.Chrome(chrome_options=chrome_options)
# chrome.get("https://2ip.ua/ru/")

def get_name():
    fname = open('resourse_files/names.txt', encoding='utf-8')
    fsurname = open('resourse_files/surnames.txt', encoding='utf-8')
    name_lines = fname.readlines()
    surname_lines = fsurname.readlines()
    print((name_lines[randint(0, 51528)].rstrip('\n') + ' ' + surname_lines[randint(0, 51528)].rstrip('\n')))
    fname.close()
    fsurname.close()
    return name_lines[randint(0, 51528)].rstrip('\n') + ' ' + surname_lines[randint(0, 51528)].rstrip('\n')


def get_email(line):
    try:
        with open('resourse_files/mailru.txt') as f:
            return f.readlines()[line].split(';')
    except IndexError:
        print('Out of emails!')


def get_proxy():
    # PROXY = None  # IP:PORT or HOST:PORT
    with open('resourse_files/proxyes.txt', 'r', encoding='utf-8') as f:
        return f.readlines()


class AutoregInst:
    def __init__(self, quantity=1):
        self.quantity = quantity
        self.num_proxy = 0
        self.num_email = 0

    def create_account(self):
        for i in range(self.quantity):
            full_name = get_name()
            password = random_word(randint(8, 10))
            email = get_email(self.num_email)
            username = random_word(randint(8, 15))
            try:
                browser, PROXY = get_chromedriver(use_proxy=True, i=self.num_proxy)
            except IndexError:
                print("ends of proxy list")
                break
            #
            # chrome_options = webdriver.ChromeOptions()
            # chrome_options.add_argument('--proxy-server=%s' % PROXY)

            # browser = webdriver.Chrome(executable_path='chromedriver.exe')
            browser.get('https://www.instagram.com/')
            time.sleep(2)

            # Email, ФИО, username, pass
            try:
                browser.find_element_by_class_name('_2hvTZ').send_keys(email[0])
            except:
                continue
            browser.find_element_by_name('fullName').send_keys(full_name)
            browser.find_element_by_name('username').send_keys(username)
            browser.find_element_by_name('password').send_keys(password)
            browser.find_element_by_xpath('.//*[@type="submit"]').click()

            # TODO check registeration
            try:
                time.sleep(0.5)
                browser.find_element_by_id('ssfErrorAlert')
            except NoSuchElementException:
                self.num_email += 1
                self.num_proxy += 1
                with open('resourse_files/instagram.txt', "a+", encoding='utf-8') as f:
                    f.write(username + ';' + password + ';' + email[0] + ';' + 'ip:' + PROXY + '\n')
                    print('ok')

        print("accounts: {0}".format(self.num_proxy))


print('Quantity of accounts:', end='')
q = input()
AutoregInst(int(q)).create_account()
input()
